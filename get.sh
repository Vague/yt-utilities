# vague Fri Jul 28 02:08:47 PM EDT 2023
# ================================================================================
# setup
# ================================================================================

# just make it a bit nicer
trap wait EXIT

already_downloaded(){
# $1 is youtube url like https://www.youtube.com/watch?v=GewTpFEg44Y
	echo ----
	echo got "$1"
	# add more slugs if have different site urls or url types
	ytslug=$(echo "$1" | cut -d'=' -f 2)
	if [ ! -z "$ytslug" ] ; then
		echo searching for $ytslug	
		ls *$ytslug*  && {
			echo found file, skipping
			return 0
		} || return 1
	fi
}

thread_limit=2
dlr="yt-dlp.sh -x "

mylog(){ echo "$@" >> /tmp/mylog.txt ; }

dl(){
	already_downloaded "$@" && return 0
	num_background_jobs=$(jobs -pr | wc -l )	
	if [ "$num_background_jobs" -ge $thread_limit ] ; then
		echo background job limit reached
		wait -n
		echo ready
	fi
	mylog "$(date) starting  + $@"
	$dlr "$@" &
}

# next
next() {
	mkdir $1
	pushd $1
	if [ ! -z "$2" ] ; then
		echo "$2" >> note.txt
	fi
}

# ready next
rn(){ 
	popd 
	next "$@"

}
# ================================================================================
# download stuff
# ================================================================================

# this lets you paste urls in and just append 'dl ' to every line starting with http
# rn is just for organization
rn download-folder 'example note' # note isnt necessary 
# you can redefine $dlr to remove -x if you want video too 
dl https://www.youtube.com/watch?v=gjx11RfLiIU

# you can also remove all calls here to rn and dl and just source this in another file


