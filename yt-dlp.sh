# vague
# run a youtubedownloader without any setup
# version2 - yt-dlp instead of youtube-dl

# max compatibility
#url="https://yt-dl.org/downloads/latest/youtube-dl"
# linux
url="https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp"
# windows
#url="https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp.exe"

path=/tmp/ytdl
get(){
        python3 "$path" --version && {
                python3 "$path" --sponsorblock-remove all --no-check-certificate --restrict-filenames $@
                return $?
        } || {
    curl -L $url -o $path
                chmod a+rx $path
                get $@
        }
} ; get $@
