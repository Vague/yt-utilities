#!/bin/bash
# vague

# run this script and copy a bunch of youtube (or other ytdl compatible urls) to the clipboard
# this script will log them all to pile.txt
# use this to feed the downloader scripts 

while sleep 1 ; do 
	xclip -out >> pile.txt
	cat pile.txt \
		| grep [a-zA-Z://=?\.0-9] | grep ^http \
		| tr -d ' ' | tr -d '\t' \
		| sort -u >> /tmp/piletemp.txt
	mv /tmp/piletemp.txt pile.txt
done
