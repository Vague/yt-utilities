#!/bin/bash
# vague
# provide a directory name to dump files into

usage(){
	echo "usage: $0 dirname <args for yt-dlp.sh>"
	exit 
}


dl="bash /home/vague/yt-utilities/yt-dlp.sh"
mkdir -pv "$1"
cd "$1" || exit 1
shift
$dl $@


