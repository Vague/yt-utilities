# vague
# Download videos in playlists exported by freetube, for offline play in better video players. Organizes into folders named for their playlist. 
# Thu Jun  6 07:59:39 PM EDT 2024

playlist_export_file='freetube-playlists-2024-06-01.db'
playlist_export_file="$1"

if [ -z "$playlist_export_file" ] ; then
	echo '$1 is the freetube exported playlist file'
	exit
fi

ytdlp="bash /home/vague/yt-utilities/yt-dlp.sh -f worst"

c=0
cat "$playlist_export_file" | while read playlist ; do 
	echo $playlist 
	playlist_name=$(echo $playlist | grep playlistName\":\"[^\"]* -o  | cut -d \" -f 3)
	mkdir -pv "$playlist_name" && pushd "$playlist_name" || exit 1
	
	echo video ids	
	ids=$(echo $playlist | grep videoId\":\"[^\"]* -o | cut -d \" -f 3 | sed -e 's_^_https://www.youtube.com/watch?v=_' )
	echo $ids | xargs $ytdlp
	
	popd
	c=$((c+1))
	echo downloaded $c playlists
done  

